from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def signup_page(request):
    return render(request, 'signup.html', {'signup_form': UserCreationForm()})

def new_user(request):
    frm = UserCreationForm(request.POST)
    if frm.is_valid():
        frm.save()
        return redirect("login")
    return HttpResponse("Signup Failed")

def login_page(request):
    return render(request, 'login.html')

def verify_user(request):
    u = request.POST.get('username')
    p = request.POST.get('password')
    usr = authenticate(username=u, password=p)
    if usr:
        login(request, usr)
        # return HttpResponse("Authenticated")
        return redirect("dashboard")
    return HttpResponse("Authentication Failed")

@login_required(login_url="login")
def dashboard_page(request):
    return render(request, 'dashboard.html')

def signout(request):
    logout(request)
    return redirect('login')